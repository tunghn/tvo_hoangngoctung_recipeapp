//
//  ShareManager.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import Foundation
import RxSwift
import RxCocoa

enum Keys: String {
    case recipe = "recipe"
}

class ShareManager {
    static let shared = ShareManager()
    let recipesByType: BehaviorRelay<[RecipeViewModel]> = BehaviorRelay(value: [])
    
    func saveRecipe(recipe: RecipeModel) {
        var recipes = getRecipes()
        recipes.append(recipe)
        setObject(.recipe, vaue: recipes)
    }
    
    func updateRecipe(recipe: RecipeModel, index: Int) {
        var recipes = getRecipes()
        recipes.remove(at: index)
        
        recipes.insert(recipe, at: index)
        setObject(.recipe, vaue: recipes)
    }
    
    func deleteRecipe(index: Int) {
        var recipes = getRecipes()
        recipes.remove(at: index)
        
        setObject(.recipe, vaue: recipes)
    }
    
    func getRecipesByType(type: String) {
        let recipes = getRecipes().filter { $0.type == type }
        var recipesViewModel: [RecipeViewModel] = []
        for recipe in recipes {
            recipesViewModel.append(RecipeViewModel(recipe: recipe))
        }
        
        self.recipesByType.accept(recipesViewModel)
    }
    
    func getRecipes() -> [RecipeModel] {
        if let data = UserDefaults.standard.value(forKey: Keys.recipe.rawValue) as? Data {
            let object = try? PropertyListDecoder().decode([RecipeModel].self, from: data)
            return object ?? []
        }
        
        return []
    }
}

extension ShareManager {
    func setObject<T: Encodable>(_ key: Keys, vaue: T) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(vaue), forKey: key.rawValue)
    }
}
