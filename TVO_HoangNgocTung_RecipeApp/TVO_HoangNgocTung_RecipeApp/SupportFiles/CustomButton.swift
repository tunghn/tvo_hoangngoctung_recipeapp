//
//  CustomButton.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit
import RxSwift
import RxCocoa

class CustomButton: UIButton {
    var isSelectedBinder = BehaviorRelay<Bool>(value: true)
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        weak var weakSelf = self
        
        self.rx.tap.subscribe { _ in
            print("TAP")
            guard let strongSelf = weakSelf else { return }
            strongSelf.isSelectedBinder.accept(!strongSelf.isSelectedBinder.value)
        }.disposed(by: self.disposeBag)
    }
}
