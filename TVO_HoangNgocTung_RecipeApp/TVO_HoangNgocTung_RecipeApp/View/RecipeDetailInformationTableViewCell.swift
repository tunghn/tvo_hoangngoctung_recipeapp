//
//  RecipeDetailInformationTableViewCell.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit

protocol RecipeDetailInformationTableViewCellDelegate {
    func didClickAddImageButton()
    func recipeName(name: String)
}

class RecipeDetailInformationTableViewCell: UITableViewCell {
    @IBOutlet weak var imageRecipeButton: UIButton!
    @IBOutlet weak var recipeTypeLabel: UILabel!
    @IBOutlet weak var recipeNameTextField: UITextField!
    var delegate: RecipeDetailInformationTableViewCellDelegate?
}

extension RecipeDetailInformationTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        recipeNameTextField.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configCellWithData(data: Any?) {
        guard let recipeViewModel = data as? RecipeViewModel else { return }
        
        imageRecipeButton.setImage(UIImage(data: recipeViewModel.dataImage), for: .normal)
        let imageRecipeTitle = recipeViewModel.dataImage.count == 0 ? "No Image" : ""
        imageRecipeButton.setTitle(imageRecipeTitle, for: .normal)
        recipeNameTextField.text = recipeViewModel.titleText
        recipeTypeLabel.text = recipeViewModel.recipeTypeText
        
    }
    
    @IBAction func clickAddImage(_ sender: UIButton) {
        delegate?.didClickAddImageButton()
    }
}

extension RecipeDetailInformationTableViewCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.recipeName(name: textField.text ?? "")
    }
}
