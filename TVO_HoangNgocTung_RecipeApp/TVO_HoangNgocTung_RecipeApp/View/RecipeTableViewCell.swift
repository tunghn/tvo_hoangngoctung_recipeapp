//
//  RecipeTableViewCell.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit

let recipeTableViewCellIdentifier = "RecipeTableViewCell"

class RecipeTableViewCell: UITableViewCell {
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!
}

extension RecipeTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    func configureWithRecipe(recipe: RecipeViewModel) {
        recipeNameLabel.text = recipe.titleText
        recipeImageView.image = UIImage(data: recipe.dataImage)
    }
}
