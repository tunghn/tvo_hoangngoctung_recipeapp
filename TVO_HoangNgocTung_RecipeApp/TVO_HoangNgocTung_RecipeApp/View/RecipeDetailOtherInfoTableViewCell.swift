//
//  RecipeDetailOtherInfoTableViewCell.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 12/04/2021.
//

import UIKit

enum RecipeOtherInfoType: Int {
    case Ingredient = 1
    case Step = 2
}

protocol RecipeDetailOtherInfoTableViewCellDelegate {
    func didClickAdd(type: RecipeOtherInfoType)
    func didClickDelete()
}

class RecipeDetailOtherInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    var indexPath: IndexPath?
    var delegate: RecipeDetailOtherInfoTableViewCellDelegate?
    var recipeViewModel: RecipeViewModel?
}

extension RecipeDetailOtherInfoTableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCellWithData(data: Any?, indexPath: IndexPath) {
        guard let recipeViewModel = data as? RecipeViewModel else { return }
        self.recipeViewModel = recipeViewModel
        self.indexPath = indexPath
        switch indexPath.section {
        case RecipeOtherInfoType.Ingredient.rawValue:
            switch indexPath.row {
            case 0:
                titleLabel.text = "Ingredients"
                rightButton.setImage(.add, for: .normal)
                break
            default:
                titleLabel.text = recipeViewModel.ingredients[indexPath.row - 1]
                rightButton.setImage(.remove, for: .normal)
                break
            }
        default:
            switch indexPath.row {
            case 0:
                titleLabel.text = "Steps"
                rightButton.setImage(.add, for: .normal)
                break
            default:
                titleLabel.text = recipeViewModel.steps[indexPath.row - 1]
                rightButton.setImage(.remove, for: .normal)
                break
            }
        }
    }
    
    private func deleteIngredient(index: Int) {
        if let recipeViewModel = recipeViewModel {
            recipeViewModel.deleteIngredient(index: index)
        }
    }
    
    private func deleteStep(index: Int) {
        if let recipeViewModel = recipeViewModel {
            recipeViewModel.deleteStep(index: index)
        }
    }
    
    @IBAction func clickRightButton(_ sender: UIButton) {
        switch indexPath?.row {
        case 0:
            delegate?.didClickAdd(type: indexPath?.section == RecipeOtherInfoType.Ingredient.rawValue ? .Ingredient : .Step)
            break
        default:
            switch indexPath?.section {
            case RecipeOtherInfoType.Ingredient.rawValue:
                deleteIngredient(index: indexPath?.row ?? 0)
                break
            default:
                deleteStep(index:  indexPath?.row ?? 0)
            }
            delegate?.didClickDelete()
            break
        }
    }
}
