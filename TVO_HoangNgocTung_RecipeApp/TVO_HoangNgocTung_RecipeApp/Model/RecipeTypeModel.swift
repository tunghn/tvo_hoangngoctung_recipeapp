//
//  RecipeTypeModel.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import Foundation

struct RecipeTypeModel {
    var recipeTypeId: Int
    var recipeTypeName: String
}
