//
//  RecipeViewModel.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit
import Foundation

class RecipeViewModel {
    var recipe: RecipeModel
    
    var titleText: String {
        return recipe.title
    }
    
    var dataImage: Data {
        return recipe.dataImage
    }
    
    var ingredients: [String] {
        return recipe.ingredients
    }
    
    var steps: [String] {
        return recipe.steps
    }
    
    var recipeTypeText: String {
        return recipe.type
    }
    
    init(recipe: RecipeModel) {
        self.recipe = recipe
    }
    
    func setDataImage(image: UIImage) {
        recipe.dataImage = image.jpegData(compressionQuality: 1.0) ?? Data()
    }
    
    func setRecipeName(name: String) {
        recipe.title = name
    }
    
    func addIngredient(str: String) {
        recipe.ingredients.append(str)
    }
    
    func addStep(str: String) {
        recipe.steps.append(str)
    }
    
    func deleteIngredient(index: Int) {
        recipe.ingredients.remove(at: index - 1)
    }
    
    func deleteStep(index: Int) {
        recipe.steps.remove(at: index - 1)
    }
}

enum RecipeDetailSection: Int {
    case Information
    case Ingredient
    case Step
}
