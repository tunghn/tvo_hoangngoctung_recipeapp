//
//  RecipeModel.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import Foundation

class RecipeModel: Codable {
    var title: String = ""
    var description: String = ""
    var type: String = ""
    var dataImage: Data = Data()
    var ingredients: [String] = []
    var steps: [String] = []
}

