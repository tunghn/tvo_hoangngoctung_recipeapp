//
//  RecipeDetailViewController.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit
import Then

enum RecipeState {
    case edit
    case new
}

class RecipeDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addRecipeButton: UIButton!
    @IBOutlet weak var otherButtonStackView: UIStackView!
    
    var imagePicker = UIImagePickerController()
    var recipeViewModel: RecipeViewModel?
    var recipeState: RecipeState = .new
    var recipeType: String = ""
    var selectedIndex = 0
}

extension RecipeDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        configView()
        createNewRecipe()
    }
}

extension RecipeDetailViewController {
    private func configView() {
        setupTableViewConfiguration()
        setupButtonConfiguration()
        
        otherButtonStackView.arrangedSubviews.forEach { (button) in
            _ = button.then {
                $0.backgroundColor = .clear
                $0.layer.cornerRadius = 6
                $0.layer.borderWidth = 1
                $0.layer.borderColor = UIColor.black.cgColor
            }
        }
        
        if recipeState == .new {
            addRecipeButton.isHidden = false
            otherButtonStackView.isHidden = true
        } else {
            addRecipeButton.isHidden = true
            otherButtonStackView.isHidden = false
        }
    }
    
    private func setupButtonConfiguration() {
        _ = addRecipeButton.then {
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 6
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    private func createNewRecipe() {
        if recipeState == .new {
            let recipe = RecipeModel()
            recipe.type = recipeType
            recipeViewModel = RecipeViewModel(recipe: recipe)
        }
    }
    private func setupTableViewConfiguration() {
        _ = tableView.then {
            $0.dataSource = self
            $0.delegate = self
            $0.bounces = false
        }
    }
    
    private func showImagePickerView() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func showAlert(type: RecipeOtherInfoType) {
        let alertController = UIAlertController(title: "Add New", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Desciption"
        }

        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { [unowned self] alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            if type == .Ingredient {
                self.recipeViewModel?.addIngredient(str: firstTextField.text ?? "")
            } else {
                self.recipeViewModel?.addStep(str: firstTextField.text ?? "")
            }
            
            self.tableView.reloadData()
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addRecipe(_ sender: UIButton) {
        if let recipeViewModel = recipeViewModel {
            ShareManager.shared.saveRecipe(recipe: recipeViewModel.recipe)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func updateRecipe(_ sender: UIButton) {
        if let recipeViewModel = recipeViewModel {
            ShareManager.shared.updateRecipe(recipe: recipeViewModel.recipe, index: selectedIndex)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteRecipe(_ sender: UIButton) {
        if let _ = recipeViewModel {
            ShareManager.shared.deleteRecipe(index: selectedIndex)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension RecipeDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case RecipeDetailSection.Information.rawValue:
            return 1
        case RecipeDetailSection.Ingredient.rawValue:
            if let recipeViewModel = recipeViewModel {
                return recipeViewModel.ingredients.count == 0 ? 1 : recipeViewModel.ingredients.count + 1
            }
            return 0
        default:
            if let recipeViewModel = recipeViewModel {
                return recipeViewModel.steps.count == 0 ? 1 : recipeViewModel.steps.count + 1
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case RecipeDetailSection.Information.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailInformationTableViewCell", for: indexPath) as? RecipeDetailInformationTableViewCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            cell.configCellWithData(data: recipeViewModel)
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailOtherInfoTableViewCell", for: indexPath) as? RecipeDetailOtherInfoTableViewCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            cell.configCellWithData(data: recipeViewModel, indexPath: indexPath)
            
            return cell
        }
    }
}

extension RecipeDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension RecipeDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}

extension RecipeDetailViewController: RecipeDetailInformationTableViewCellDelegate {
    func didClickAddImageButton() {
        ImagePickerManager().pickImage(self) { [unowned self] (image, url) in
            self.recipeViewModel?.setDataImage(image: image)
            self.tableView.reloadData()
        }
    }
    
    func recipeName(name: String) {
        self.recipeViewModel?.setRecipeName(name: name)
    }
}

extension RecipeDetailViewController: RecipeDetailOtherInfoTableViewCellDelegate {
    func didClickAdd(type: RecipeOtherInfoType) {
        showAlert(type: type)
    }
    
    func didClickDelete() {
        tableView.reloadData()
    }
}
