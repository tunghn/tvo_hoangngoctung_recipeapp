//
//  RecipesViewController.swift
//  TVO_HoangNgocTung_RecipeApp
//
//  Created by Hoang Ngoc Tung on 11/04/2021.
//

import UIKit
import RxSwift
import RxCocoa
import Then

class RecipesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recipeTypeButton: CustomButton!
    @IBOutlet weak var addRecipeButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var containPickerView: UIView!
    let disposeBag = DisposeBag()
    var recipeId = Int()
    var recipeType = String()
    var elementName = String()
    var recipeTypes: [RecipeTypeModel] = []
    var selectedType: String = "" {
        didSet {
            ShareManager.shared.getRecipesByType(type: selectedType)
        }
    }
}

extension RecipesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Recipes"
        
        getAllRecipeTypes()
        setupButtonConfiguration()
        setupPickerViewConfiguration()
        setupCellConfiguration()
        setupCellTabHandling()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ShareManager.shared.getRecipesByType(type: selectedType)
    }
}

extension RecipesViewController {
    private func getAllRecipeTypes() {
        if let path = Bundle.main.url(forResource: "recipetypes", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
    }
    
    private func setupPickerViewConfiguration() {
        let names = recipeTypes.map {$0.recipeTypeName}
        let recipeTypeNames: Observable<[String]> = Observable.of(names)
        recipeTypeNames
            .bind(to: pickerView
                    .rx
                    .itemTitles) { (row, element) in
                return element
            }
            .disposed(by: disposeBag)
        
        pickerView
            .rx
            .itemSelected
            .subscribe(onNext: { [unowned self] (row, value) in
                let type = self.recipeTypes[row].recipeTypeName
                self.selectedType = type
                self.recipeTypeButton.setTitle(type, for: .normal)
                self.recipeTypeButton.isSelected = !self.recipeTypeButton.isSelected
                self.containPickerView.isHidden = !self.recipeTypeButton.isSelected
            })
            .disposed(by: disposeBag)
    }
    
    private func setupButtonConfiguration() {
        _ = addRecipeButton.then {
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 6
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.black.cgColor
        }
        
        _ = recipeTypeButton.then {
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 6
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.black.cgColor
        }
        
        addRecipeButton
            .rx
            .tap
            .bind { [unowned self] in
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeDetailViewController") as? RecipeDetailViewController {
                    vc.recipeState = .new
                    vc.recipeType = selectedType
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            .disposed(by: disposeBag)
        
        recipeTypeButton
            .rx
            .tap
            .bind { [unowned self] in
                self.recipeTypeButton.isSelected = !self.recipeTypeButton.isSelected
                self.containPickerView.isHidden = !self.recipeTypeButton.isSelected
            }
            .disposed(by: disposeBag)
    }
    
    private func setupCellConfiguration() {
        ShareManager.shared.recipesByType.asObservable()
            .bind(to: tableView
                    .rx
                    .items(cellIdentifier: recipeTableViewCellIdentifier,
                           cellType: RecipeTableViewCell.self)) { row, recipe, cell in
                cell.configureWithRecipe(recipe: recipe)
            }
            .disposed(by: disposeBag)
    }
    
    private func setupCellTabHandling() {
        tableView
            .rx
            .modelSelected(RecipeViewModel.self)
            .subscribe(onNext: { [unowned self] recipe in
                print(recipe)
                if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeDetailViewController") as? RecipeDetailViewController {
                        vc.recipeState = .edit
                        vc.recipeType = selectedType
                        vc.recipeViewModel = recipe
                        vc.selectedIndex = selectedRowIndexPath.row
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
                
            })
            .disposed(by: disposeBag)
    }
}

extension RecipesViewController: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if elementName == "type" {
            recipeId = Int()
            recipeType = String()
        }

        self.elementName = elementName
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "type" {
            let recipeTypeModel = RecipeTypeModel(recipeTypeId: recipeId, recipeTypeName: recipeType)
            recipeTypes.append(recipeTypeModel)
            selectedType = recipeTypes[0].recipeTypeName
            recipeTypeButton.setTitle(selectedType, for: .normal)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if (!data.isEmpty) {
            if self.elementName == "id" {
                recipeId += Int(data) ?? 0
            } else if self.elementName == "name" {
                recipeType += data
            }
        }
    }
}
