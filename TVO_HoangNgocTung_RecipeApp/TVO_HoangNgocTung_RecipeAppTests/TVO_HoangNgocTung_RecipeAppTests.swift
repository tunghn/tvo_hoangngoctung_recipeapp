//
//  TVO_HoangNgocTung_RecipeAppTests.swift
//  TVO_HoangNgocTung_RecipeAppTests
//
//  Created by Hoang Ngoc Tung on 10/04/2021.
//

import XCTest
@testable import TVO_HoangNgocTung_RecipeApp

class TVO_HoangNgocTung_RecipeAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRecipe() throws {
        let recipe = RecipeModel()
        recipe.title = "Recipe name"
        recipe.type = "Type 1"
        recipe.ingredients = ["rice", "fish sauce"]
        recipe.steps = ["step1", "step2"]
        let recipeViewModel = RecipeViewModel(recipe: recipe)
        XCTAssertEqual(recipeViewModel.titleText, "Recipe name")
        XCTAssertEqual(recipeViewModel.recipeTypeText, "Type 2")
        XCTAssertEqual(recipeViewModel.ingredients, ["rice", "fish sauce"])
        XCTAssertEqual(recipeViewModel.steps, ["step2", "step1"])
    }

}
